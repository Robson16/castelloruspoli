<?php

/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header pt-5">
		<figure title="<?php the_title_attribute(); ?>">
			<?php
			the_post_thumbnail('medium_large', array(
				'class' => 'post-thumbnail img-fluid pt-5',
				'title' => get_the_title()
			));
			?>
		</figure>
		<?php the_title('<h1 class="entry-title">', '</h1>'); ?>
		<p>
			<i class="dashicons dashicons-admin-users"></i>
			<span><?php the_author_posts_link(); ?>&nbsp;</span>

			<i class="dashicons dashicons-calendar-alt"></i>
			<span><?php echo get_the_date(); ?>&nbsp;</span>

			<i class="dashicons dashicons-category"></i>
			<span><?php the_category(', '); ?>&nbsp;</span>

			<?php if (has_tag()) : ?>
				<i class="dashicons dashicons-tag"></i>
				<span><?php the_tags('', ', ', ''); ?>&nbsp;</span>
			<? endif; ?>

			<?php if (get_comments_number()) : ?>
				<i class="dashicons dashicons-admin-comments"></i>
				<span><?php echo get_comments_number(); ?></span>
			<? endif; ?>
		</p>
	</header>
	<!-- /.entry-header -->

	<div class="entry-content">
		<?php the_content(); ?>
	</div>
	<!-- /.entry-content -->
</article><!-- #post-<?php the_ID(); ?> -->