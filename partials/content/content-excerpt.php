<?php

/**
 * Template part for displaying post archives and search results
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class('mb-4 pb-4'); ?>>
	<header class="entry-header">
		<a class="post-thumbnail" href="<?php echo esc_url(get_permalink()); ?>">
			<figure title="<?php the_title_attribute(); ?>">
				<?php
				the_post_thumbnail('medium_large', array(
					'class' => 'img-fluid',
					'title' => get_the_title()
				));
				?>
			</figure>
		</a>
		<?php
		if (is_sticky() && is_home() && !is_paged()) {
			printf('<span class="sticky-post">%s</span>', _x('Post', 'Featured', 'castelloruspoli'));
		}
		the_title(sprintf('<h3 class="entry-title"><a href="%s" rel="bookmark">', esc_url(get_permalink())), '</a></h3>');
		?>
		<p>
			<i class="dashicons dashicons-admin-users"></i>
			<span><?php the_author_posts_link(); ?>&nbsp;</span>

			<i class="dashicons dashicons-calendar-alt"></i>
			<span><?php echo get_the_date(); ?>&nbsp;</span>

			<i class="dashicons dashicons-category"></i>
			<span><?php the_category(', '); ?>&nbsp;</span>

			<?php if (has_tag()) : ?>
				<i class="dashicons dashicons-tag"></i>
				<span><?php the_tags('', ', ', ''); ?>&nbsp;</span>
			<? endif; ?>

			<?php if (get_comments_number()) : ?>
				<i class="dashicons dashicons-admin-comments"></i>
				<span><?php echo get_comments_number(); ?></span>
			<? endif; ?>
		</p>
	</header>
	<!-- /.entry-header -->

	<div class="entry-content">
		<p><?php echo wp_trim_words(get_the_content(), 35); ?></p>
	</div>
	<!-- /.entry-content -->

	<footer class="entry-footer">
		<a class="btn read-more-link" href="<?php echo esc_url(get_permalink()); ?>"><?php _e('Read more', 'castelloruspoli') ?></a>
	</footer>
	<!-- /.entry-footer -->
</article><!-- #post-<?php the_ID(); ?> -->