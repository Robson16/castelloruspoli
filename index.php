<?php

/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 */
get_header();
?>

<main>
	<div class="container py-5">
		<div class="row pt-5">
			<div class="col-12 offset-lg-2 col-lg-8 pt-5">

				<?php
				if (is_search()) echo sprintf('<h1 class="d-inline-block display-4">%s <span>%s</span></h1>', __('Search results for:', 'castelloruspoli'), get_search_query());
				if (is_archive()) echo sprintf('<h1 class="display-4">%s</h1>', get_the_archive_title());
				if (is_home()) echo '<h1 class="display-4">Blog</h1>';

				if (have_posts()) {
					while (have_posts()) {
						the_post();
						get_template_part('partials/content/content', 'excerpt');
					}

					the_posts_pagination();
				} else {
					get_template_part('partials/content/content', 'none');
				}
				?>

			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</div>
	<!--/.container-->
</main>

<?php
get_footer();
