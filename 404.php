<?php

/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 */
get_header();
?>

<main>
	<section class="container d-flex flex-column justify-content-center" style="min-height: 100vh;">
		<h1 class="page-title"><?php _e("Can't find this page", 'castelloruspoli'); ?></h1>
		<p><?php _e('It looks like nothing was found at this location', 'castelloruspoli'); ?></p>
		<a class="h2 font-weight-bold" href="<?php echo get_home_url(); ?>"><?php _e('Return to home page', 'castelloruspoli'); ?></a>
	</section>
</main>

<?php
get_footer();
