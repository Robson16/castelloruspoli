<?php

/**
 *  Sidebar Blog
 *
 */
?>

<aside class="blog-sidebar">
	<?php if (is_active_sidebar('castelloruspoli-sidebar-blog')) dynamic_sidebar('castelloruspoli-sidebar-blog'); ?>
</aside>