# Castello Ruspoli

<p align="center">
    <img alt="screenshot" title="Screenshot" src="./screenshot.png" />
</p>

This is a custom WordPress theme developed for the historic [Castello Ruspoli](https://www.castelloruspoli.com/).

## About Castello Ruspoli

The first traces of Ruspoli Castle date back to 847, when it was first built on the cliff where it still stands. During the papacy of Leo IV (847-855), the structure was transformed into a Benedictine monastery and remained so until 1081. By 1169, it became the center of a prolonged dispute between powerful families such as the Aldobrandini, Orsini, and Borgia, which lasted until the 16th century.

In 1531, Pope Clement VII gifted the castle to Beatrice Farnese Baglioni. Afterward, it came into the possession of the Marescotti family, who continued to shape its legacy. The garden of Castello Ruspoli, designed by Marcantonio Marescotti and Ottavia Orsini in the early 17th century, is regarded as one of Italy's most significant Renaissance gardens and remains well-preserved to this day.

## Getting started

Download the code from this repository and place it in a folder inside your WordPress installation themes folder, like this path:

```
\wp-content\themes\castelloruspoli

```

Then you will see and be able to activate the theme on your WordPress dashboard > Appearance > Themes

## 🛠 Technologies
This project was developed with the following technologies

- [WordPress](https://br.wordpress.org/)
- [SASS](https://sass-lang.com/)
- [Node.js](https://nodejs.org/)
- [Webpack](https://webpack.js.org/)
- [Babel](https://babeljs.io/)

## 👨‍💻 Editing the code

If you want to make additions to this project code, like the CSS or Js, is recommended to have [Node.js](https://nodejs.org/) installed
and configured, so you can use it to compile [SASS](https://sass-lang.com/) into CSS and make the JavaScript well organized and light.

Open this code in your favorite editor, use the NPM or YARN packages managers to install all developments dependencies and run the scripts on the command line:

## 👉 For compile SASS:

```
npm run watch:sass

```
or
```
yarn watch:sass

```
## 👉 For compile JavaScript:

```
npm run watch:js

```
or
```
yarn watch:js

```

---

### ☕❤

[Robson H. Rodrigues](https://www.linkedin.com/in/robson-h-rodrigues-93341746/)
