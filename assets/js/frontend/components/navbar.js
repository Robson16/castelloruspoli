export default class Navbar {
	constructor() {
		this.wpadminbar = document.querySelector( '#wpadminbar' );
		this.header = document.querySelector( '#header' );
		this.topbar = document.querySelector( '#topbar' );
		this.navbar = document.querySelector( '#navbar' );
		this.main = document.querySelector( 'main' );
		this.viwewportX = document.documentElement.clientWidth;
		this.offset = this.topbar.offsetHeight;
		this.events();
	}

	// Events
	events() {
		this.handleMainContentSpaceTop();

		window.addEventListener( 'scroll', () => {
			this.handleStickEffect();
			this.handleWhenLoggedIn();
		} );

		window.addEventListener( 'resize', () => {
			this.handleViwewportSize();
			this.handleMainContentSpaceTop();
		} );
	}

	// Methods
	handleStickEffect() {
		if ( window.scrollY > this.offset ) {
			this.header.classList.add( 'sticky' );
		} else {
			this.header.classList.remove( 'sticky' );
		}
	}

	handleWhenLoggedIn() {
		// When the WP Admin Bar is visible, we need to change the top space to stick the navbar menu visible below it
		if ( window.scrollY > this.offset ) {
			if ( this.wpadminbar && this.viwewportX > 576 )
				this.navbar.style.top = this.wpadminbar.offsetHeight + 'px';
		} else {
			if ( this.wpadminbar && this.viwewportX > 576 )
				this.navbar.style.top = 'initial';
		}
	}

	handleViwewportSize() {
		this.viwewportX = document.documentElement.clientWidth;
	}

	handleMainContentSpaceTop() {
		this.main.style.paddingTop = this.header.offsetHeight + 'px';
	}
}
