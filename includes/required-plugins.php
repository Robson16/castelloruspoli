<?php

/**
 * Include the TGM_Plugin_Activation class.
 *
 */
require_once get_template_directory() . '/includes/classes/class-tgm-plugin-activation.php';

add_action('tgmpa_register', 'castelloruspoli_register_required_plugins');

/**
 * Register the required plugins for this theme.
 *
 */
function castelloruspoli_register_required_plugins()
{
	/*
	 * Array of plugin arrays. Required keys are name and slug.
	 * If the source is NOT from the .org repo, then source is also required.
	 */
	$plugins = array(
		// These are plugin from the WordPress Plugin Repository.
		array(
			'name'      => 'Kirki Customizer Framework',
			'slug'      => 'kirki',
			'required'  => true,
		),
		array(
			'name'      => 'Contact Form 7',
			'slug'      => 'contact-form-7',
			'required'  => false,
		),
		array(
			'name'      => 'Flamingo',
			'slug'      => 'flamingo',
			'required'  => false,
		),
		array(
			'name'      => 'Easy FancyBox',
			'slug'      => 'easy-fancybox',
			'required'  => false,
		),
		array(
			'name'      => 'WooCommerce',
			'slug'      => 'woocommerce',
			'required'  => false,
		),
		array(
			'name'      => 'Font Awesome',
			'slug'      => 'font-awesome',
			'required'  => true,
		),
		array(
			'name'      => 'Polylang',
			'slug'      => 'polylang',
			'required'  => false,
		),

		// These are plugin bundled with a theme.
		array(
			'name'               => 'Bookings and Appointments For WooCommerce Premium', // The plugin name.
			'slug'               => 'ph-bookings-appointments-woocommerce-premium', // The plugin slug (typically the folder name).
			'source'             => get_template_directory() . '/lib/plugins/ph-bookings-appointments-woocommerce-premium.zip', // The plugin source.
			'required'           => false, // If false, the plugin is only 'recommended' instead of required.
			'version'            => '', // E.g. 1.0.0. If set, the active plugin must be this version or higher. If the plugin version is higher than the plugin version installed, the user will be notified to update the plugin.
			'force_activation'   => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch.
			'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins.
			'external_url'       => '', // If set, overrides default API URL and points to an external URL.
			'is_callable'        => '', // If set, this callable will be be checked for availability to determine if a plugin is active.
		),

		array(
			'name'               => 'Polylang for WooCommerce', // The plugin name.
			'slug'               => 'polylang-wc', // The plugin slug (typically the folder name).
			'source'             => get_template_directory() . '/lib/plugins/polylang-wc.zip', // The plugin source.
			'required'           => false, // If false, the plugin is only 'recommended' instead of required.
			'version'            => '', // E.g. 1.0.0. If set, the active plugin must be this version or higher. If the plugin version is higher than the plugin version installed, the user will be notified to update the plugin.
			'force_activation'   => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch.
			'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins.
			'external_url'       => '', // If set, overrides default API URL and points to an external URL.
			'is_callable'        => '', // If set, this callable will be be checked for availability to determine if a plugin is active.
		),
	);

	/*
	 * Array of configuration settings. Amend each line as needed.
	 *
	 */
	$config = array(
		'id'           => 'castelloruspoli',              // Unique ID for hashing notices for multiple instances of TGMPA.
		'default_path' => '',                      // Default absolute path to bundled plugins.
		'menu'         => 'tgmpa-install-plugins', // Menu slug.
		'parent_slug'  => 'themes.php',            // Parent menu slug.
		'capability'   => 'edit_theme_options',    // Capability needed to view plugin install page, should be a capability associated with the parent menu used.
		'has_notices'  => true,                    // Show admin notices or not.
		'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
		'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
		'is_automatic' => true,                    // Automatically activate plugins after installation or not.
		'message'      => '',                      // Message to output right before the plugins table.


		'strings'      => array(
			'page_title'                      => __('Install Required Plugins', 'castelloruspoli'),
			'menu_title'                      => __('Install Plugins', 'castelloruspoli'),
			/* translators: %s: plugin name. */
			'installing'                      => __('Installing Plugin: %s', 'castelloruspoli'),
			/* translators: %s: plugin name. */
			'updating'                        => __('Updating Plugin: %s', 'castelloruspoli'),
			'oops'                            => __('Something went wrong with the plugin API.', 'castelloruspoli'),
			'notice_can_install_required'     => _n_noop(
				/* translators: 1: plugin name(s). */
				'This theme requires the following plugin: %1$s.',
				'This theme requires the following plugins: %1$s.',
				'castelloruspoli'
			),
			'notice_can_install_recommended'  => _n_noop(
				/* translators: 1: plugin name(s). */
				'This theme recommends the following plugin: %1$s.',
				'This theme recommends the following plugins: %1$s.',
				'castelloruspoli'
			),
			'notice_ask_to_update'            => _n_noop(
				/* translators: 1: plugin name(s). */
				'The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.',
				'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.',
				'castelloruspoli'
			),
			'notice_ask_to_update_maybe'      => _n_noop(
				/* translators: 1: plugin name(s). */
				'There is an update available for: %1$s.',
				'There are updates available for the following plugins: %1$s.',
				'castelloruspoli'
			),
			'notice_can_activate_required'    => _n_noop(
				/* translators: 1: plugin name(s). */
				'The following required plugin is currently inactive: %1$s.',
				'The following required plugins are currently inactive: %1$s.',
				'castelloruspoli'
			),
			'notice_can_activate_recommended' => _n_noop(
				/* translators: 1: plugin name(s). */
				'The following recommended plugin is currently inactive: %1$s.',
				'The following recommended plugins are currently inactive: %1$s.',
				'castelloruspoli'
			),
			'install_link'                    => _n_noop(
				'Begin installing plugin',
				'Begin installing plugins',
				'castelloruspoli'
			),
			'update_link' 					  => _n_noop(
				'Begin updating plugin',
				'Begin updating plugins',
				'castelloruspoli'
			),
			'activate_link'                   => _n_noop(
				'Begin activating plugin',
				'Begin activating plugins',
				'castelloruspoli'
			),
			'return'                          => __('Return to Required Plugins Installer', 'castelloruspoli'),
			'plugin_activated'                => __('Plugin activated successfully.', 'castelloruspoli'),
			'activated_successfully'          => __('The following plugin was activated successfully:', 'castelloruspoli'),
			/* translators: 1: plugin name. */
			'plugin_already_active'           => __('No action taken. Plugin %1$s was already active.', 'castelloruspoli'),
			/* translators: 1: plugin name. */
			'plugin_needs_higher_version'     => __('Plugin not activated. A higher version of %s is needed for this theme. Please update the plugin.', 'castelloruspoli'),
			/* translators: 1: dashboard link. */
			'complete'                        => __('All plugins installed and activated successfully. %1$s', 'castelloruspoli'),
			'dismiss'                         => __('Dismiss this notice', 'castelloruspoli'),
			'notice_cannot_install_activate'  => __('There are one or more required or recommended plugins to install, update or activate.', 'castelloruspoli'),
			'contact_admin'                   => __('Please contact the administrator of this site for help.', 'castelloruspoli'),

			'nag_type'                        => '', // Determines admin notice type - can only be one of the typical WP notice classes, such as 'updated', 'update-nag', 'notice-warning', 'notice-info' or 'error'. Some of which may not work as expected in older WP versions.
		),
	);

	tgmpa($plugins, $config);
}
