<?php

new \Kirki\Section(
	'section_social_networks',
	array(
		'title'       => esc_html__('Social Networks', 'castelloruspoli'),
		'description' => esc_html__('Social networks to use around the site, like on the WooCommerce Emails footer.', 'castelloruspoli'),
		'priority'    => 160,
	)
);

new \Kirki\Field\URL(
	array(
		'settings' => 'setting_instagram',
		'label'    => esc_html__('Instagram Page URL', 'castelloruspoli'),
		'section'  => 'section_social_networks',
		'default'  => 'https://www.instagram.com/',
		'priority' => 10,
	)
);

new \Kirki\Field\URL(
	array(
		'settings' => 'setting_facebook',
		'label'    => esc_html__('Facebook Page URL', 'castelloruspoli'),
		'section'  => 'section_social_networks',
		'default'  => 'https://www.facebook.com/',
		'priority' => 10,
	)
);

new \Kirki\Field\URL(
	array(
		'settings' => 'setting_tripadvisor',
		'label'    => esc_html__('Tripadvisor Page URL', 'castelloruspoli'),
		'section'  => 'section_social_networks',
		'default'  => 'https://www.tripadvisor.com.br/',
		'priority' => 10,
	)
);

new \Kirki\Field\URL(
	array(
		'settings' => 'setting_vimeo',
		'label'    => esc_html__('Vimeo Page URL', 'castelloruspoli'),
		'section'  => 'section_social_networks',
		'default'  => 'https://vimeo.com/',
		'priority' => 10,
	)
);
