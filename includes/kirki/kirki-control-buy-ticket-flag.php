<?php

new \Kirki\Section(
	'section_buyticketflag',
	array(
		'title'          => esc_html__('Buy Ticket Floating Flag', 'castelloruspoli'),
		'description'    => esc_html__('Add which pages will have a floating flag with a link to buy the ticket to visit Castello Ruspoli.', 'castelloruspoli'),
		'priority'       => 160,
	)
);


new \Kirki\Field\Repeater(
	array(
		'label'       => esc_html__('Pages with buy ticket floating flag', 'castelloruspoli'),
		'section'     => 'section_buyticketflag',
		'priority'    => 10,
		'row_label' => array(
			'type'  => 'field',
			'value' => esc_html__('Page', 'castelloruspoli'),
			'field' => 'link_text',
		),
		'button_label' => esc_html__('Add another page', 'castelloruspoli'),
		'settings'     => 'buyticketflag_setting',
		'default'      => array(
			array(
				'page_id' 	=> 42,
				'link_url'  => 'https://exemple.com/',
				'new_tab'	=> false,
			),
		),
		'fields' => array(
			'page_id' => array(
				'type'        => 'dropdown-pages',
				'settings'    => 'dropdown_pages_setting',
				'label'       => esc_html__('Dropdown Pages', 'castelloruspoli'),
				'default'     => 42,
			),
			'link_url' => array(
				'type'        => 'text',
				'label'       => esc_html__('Link URL', 'castelloruspoli'),
				'description' => esc_html__('This will be the link URL', 'castelloruspoli'),
				'default'     => '',
			),
			'new_tab' => array(
				'type'        => 'checkbox',
				'label'       => esc_html__('Open in new tab', 'castelloruspoli'),
				'default'     => false,
			),
		)
	)
);
