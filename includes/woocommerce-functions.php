<?php

/**
 * Woocommerce Functions
 *
 */

add_action('wp_enqueue_scripts', 'castelloruspoli_woocommerce_scripts');
function castelloruspoli_woocommerce_scripts()
{
	// CSS
	wp_enqueue_style('castelloruspoli-woocommerce-styles', get_template_directory_uri() . '/assets/css/frontend/woocommerce-styles.css', array(), wp_get_theme()->get('Version'));
}

// Register Woocommerce defaults and support for various WordPress features.
add_action('after_setup_theme', 'castelloruspoli_woocommerce_setup');
function castelloruspoli_woocommerce_setup()
{
	// Enable basic support for WooCommerce
	add_theme_support('woocommerce');

	// Enable product gallery slider
	add_theme_support('wc-product-gallery-slider');
}

add_filter('woocommerce_single_product_carousel_options', 'castelloruspoli_woocommerce_flexslider_options');
function castelloruspoli_woocommerce_flexslider_options($options)
{
	/**
	 * Show arrows
	 */
	$options['directionNav'] = true;
	$options['animation'] = "slide";

	/**
	 * Infinite loop
	 */
	$options['animationLoop'] = true;

	/**
	 * Autoplay (work with only slideshow too)
	 */
	$options['slideshow'] = true;
	$options['autoplay'] = true;

	return $options;
}
