<?php

/**
 * The template for displaying the footer
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 */

?>

<footer class="footer">
	<?php
	/**
	 * Show the buy ticket floating flag only if is one of the pages indicated in the customizer
	 */
	$currentPageId = get_the_ID();
	$pagesArray = get_theme_mod('buyticketflag_setting');
	foreach ($pagesArray as $pageItem) :
		if ($pageItem['page_id'] == $currentPageId) : ?>
			<a class="buy-now-flag" href="<?php echo $pageItem['link_url'] ?>" target="<?php echo ($pageItem['new_tab']) ? '_blank' : '_self'; ?>">
				<?php esc_html_e('Buy now', 'castelloruspoli'); ?>
			</a>
	<?php
			break;
		endif;
	endforeach;
	?>

	<div class="footer-widgets">
		<div class="container">
			<?php if (is_active_sidebar('castelloruspoli-sidebar-footer-1')) : ?>
				<div class="widget-column">
					<?php dynamic_sidebar('castelloruspoli-sidebar-footer-1'); ?>
				</div>
			<?php endif; ?>
			<?php if (is_active_sidebar('castelloruspoli-sidebar-footer-2')) : ?>
				<div class="widget-column">
					<?php dynamic_sidebar('castelloruspoli-sidebar-footer-2'); ?>
				</div>
			<?php endif; ?>
			<?php if (is_active_sidebar('castelloruspoli-sidebar-footer-3')) : ?>
				<div class="widget-column">
					<?php dynamic_sidebar('castelloruspoli-sidebar-footer-3'); ?>
				</div>
			<?php endif; ?>
			<?php if (is_active_sidebar('castelloruspoli-sidebar-footer-4')) : ?>
				<div class="widget-column">
					<?php dynamic_sidebar('castelloruspoli-sidebar-footer-4'); ?>
				</div>
			<?php endif; ?>
		</div>
		<!-- /.container -->
	</div>
	<!-- /.footer-widgets -->

	<div class="footer-bottom">
		<div class="container">
			<span class="footer-copyright">&copy;&nbsp;<?php echo wp_date('Y'); ?>&nbsp;<?php echo bloginfo('title'); ?>&nbsp;<?php _e('All rights reserved.', 'castelloruspoli') ?></span>
			<span class="footer-developer">
				<?php
				$castelloruspoli_developer_logo = sprintf('<img src="%s" alt="%s" title="%s">', get_template_directory_uri() . '/assets/images/vollup-logo.png', 'Vollup', 'Vollup');
				$castelloruspoli_developer_link = sprintf('<a href="%s" target="_blank">%s</a>', 'https://www.vollup.com/', $castelloruspoli_developer_logo);

				echo sprintf("%s %s.", esc_html__('Developed by:', 'castelloruspoli'), $castelloruspoli_developer_link);
				?>
			</span>
		</div>
		<!-- /.container -->
	</div>
	<!-- /.footer-copyright -->
</footer>

<?php wp_footer(); ?>

</body>

</html>
