<?php

/**
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 */

// Enqueue Front-End
function castelloruspoli_scripts()
{
	// CSS
	wp_enqueue_style('bootstrap', '//stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css', null, '4.5.0', 'all');
	wp_enqueue_style('castelloruspoli-shared-styles', get_template_directory_uri() . '/assets/css/shared/shared-styles.css', array(), wp_get_theme()->get('Version'));
	wp_enqueue_style('castelloruspoli-frontend-styles', get_template_directory_uri() . '/assets/css/frontend/frontend-styles.css', array(), wp_get_theme()->get('Version'));

	// Js
	wp_deregister_script('jquery');
	wp_register_script('jquery', '//ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js', array(), '3.5.1', true);
	wp_enqueue_script('jquery');
	wp_enqueue_script('comment-reply');
	wp_enqueue_script('popper', '//cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js', array('jquery'), '1.16.0', true);
	wp_enqueue_script('bootstrap', '//stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js', array('jquery', 'popper'), '4.5.0', true);
	wp_enqueue_script('castelloruspoli-frontend-script', get_template_directory_uri() . '/assets/js/frontend/frontend-bundled.js', array(), wp_get_theme()->get('Version'), true);
}
add_action('wp_enqueue_scripts', 'castelloruspoli_scripts');

/**
 * Set theme defaults and register support for various WordPress features.
 */
function castelloruspoli_setup()
{
	// Enabling translation support
	$textdomain = 'castelloruspoli';
	load_theme_textdomain($textdomain, get_stylesheet_directory() . '/languages/');
	load_theme_textdomain($textdomain, get_template_directory() . '/languages/');

	// Customizable logo
	add_theme_support('custom-logo', array(
		'height'      => 52,
		'width'       => 80,
		'flex-height' => false,
		'flex-width'  => false,
		'header-text' => array('site-title', 'site-description'),
	));

	// Menu registration
	register_nav_menus(array(
		'topbar_menu_left' => __('Topbar Left', 'castelloruspoli'),
		'topbar_menu_right' => __('Topbar Right', 'castelloruspoli'),
		'main_menu' => __('Main Menu', 'castelloruspoli'),
	));

	// Load custom styles in the editor.
	add_theme_support('editor-styles');
	add_editor_style(get_stylesheet_directory_uri() . '/assets/css/shared/shared-styles.css');
	add_editor_style(get_stylesheet_directory_uri() . '/assets/css/admin/admin-styles.css');

	// Let WordPress manage the document title.
	add_theme_support('title-tag');

	// Enable support for featured image on posts and pages.
	add_theme_support('post-thumbnails');

	// Enable support for embedded media for full weight
	add_theme_support('responsive-embeds');

	// Enables wide and full dimensions
	add_theme_support('align-wide');

	// Standard style for each block.
	add_theme_support('wp-block-styles');

	/**
	 * Custom blocks styles.
	 *
	 * @see https://wpblockz.com/tutorial/register-block-styles-in-wordpress/
	 * @link https://developer.wordpress.org/block-editor/reference-guides/filters/block-filters/
	 */
	register_block_style('core/columns', array(
		'name' => 'timeline-columns',
		'label' => __('Timeline Columns', 'castelloruspoli'),
	));
}

add_action('after_setup_theme', 'castelloruspoli_setup');

/**
 * Registration of widget areas.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function castelloruspoli_sidebars()
{
	// Args used in all calls register_sidebar().
	$shared_args = array(
		'before_title' => '<h4 class="widget-title">',
		'after_title' => '</h4>',
		'before_widget' => '<div class="widget %2$s"><div class="widget-content">',
		'after_widget' => '</div></div>',
	);

	// Footer #1
	register_sidebar(array_merge($shared_args, array(
		'name' => __('Footer #1', 'castelloruspoli'),
		'id' => 'castelloruspoli-sidebar-footer-1',
		'description' => __('The widgets in this area will be displayed in the first column in Footer.', 'castelloruspoli'),
	)));

	// Footer #2
	register_sidebar(array_merge($shared_args, array(
		'name' => __('Footer #2', 'castelloruspoli'),
		'id' => 'castelloruspoli-sidebar-footer-2',
		'description' => __('The widgets in this area will be displayed in the second column in Footer.', 'castelloruspoli'),
	)));

	// Footer #3
	register_sidebar(array_merge($shared_args, array(
		'name' => __('Footer #3', 'castelloruspoli'),
		'id' => 'castelloruspoli-sidebar-footer-3',
		'description' => __('The widgets in this area will be displayed in the third column in Footer.', 'castelloruspoli'),
	)));

	// Footer #4
	register_sidebar(array_merge($shared_args, array(
		'name' => __('Footer #4', 'castelloruspoli'),
		'id' => 'castelloruspoli-sidebar-footer-4',
		'description' => __('The widgets in this area will be displayed in the fourth column in Footer.', 'castelloruspoli'),
	)));

	// Barra Lateral Blog #1
	register_sidebar(array_merge($shared_args, array(
		'name' => __('Sidebar Blog', 'castelloruspoli'),
		'id' => 'castelloruspoli-sidebar-blog',
		'description' => __('The widgets in this area will be displayed in the blog sidebar.', 'castelloruspoli'),
	)));
}

add_action('widgets_init', 'castelloruspoli_sidebars');

/**
 *  WordPress Bootstrap Nav Walker
 */
require_once get_template_directory() . '/includes/classes/class-wp-bootstrap-navwalker.php';

/**
 *  TGM Plugin
 */
require_once get_template_directory() . '/includes/required-plugins.php';

/**
 *  Kirki Framework Config
 */
require_once get_template_directory() . '/includes/kirki/kirki-config.php';

/**
 * 	Woocommerce Function
 */
if (class_exists('woocommerce')) {
	require_once get_template_directory() . '/includes/woocommerce-functions.php';
}
