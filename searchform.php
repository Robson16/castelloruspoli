<?php

/**
 * Template for displaying search forms
 *
 */
?>

<form role="search" method="get" class="search-form form-inline w-100" action="<?php echo esc_url(home_url('/')); ?>">

	<label class="search-label d-none" for="s"><?php _e('Search', 'castelloruspoli'); ?></label>

	<div class="input-group flex-grow-1">
		<input type="search" id="s" name="s" class="search-field form-control flex-grow-1" placeholder="<?php _e('Search for', 'castelloruspoli') ?>" value="<?php echo get_search_query(); ?>" aria-label="<?php _e('Search for', 'castelloruspoli'); ?>" />

		<div class="input-group-append">
			<button class="search-submit" type="submit"><?php _e('Search!', 'castelloruspoli'); ?></button>
		</div>
	</div>
</form>